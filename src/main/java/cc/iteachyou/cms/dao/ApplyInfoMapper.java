package cc.iteachyou.cms.dao;

import java.util.List;
import java.util.Map;

import cc.iteachyou.cms.common.BaseMapper;
import cc.iteachyou.cms.entity.ApplyInfo;

public interface ApplyInfoMapper extends BaseMapper<ApplyInfo> {

	List<ApplyInfo> queryListByPage(Map<String, Object> entity);
	
}