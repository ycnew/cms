package cc.iteachyou.cms.dao;

import java.util.List;
import java.util.Map;

import cc.iteachyou.cms.common.BaseMapper;
import cc.iteachyou.cms.entity.MessageInfo;

public interface MessageInfoMapper extends BaseMapper<MessageInfo> {

	List<MessageInfo> queryListByPage(Map<String, Object> entity);
	
}