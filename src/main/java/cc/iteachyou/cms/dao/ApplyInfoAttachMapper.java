package cc.iteachyou.cms.dao;

import java.util.List;
import java.util.Map;

import cc.iteachyou.cms.common.BaseMapper;
import cc.iteachyou.cms.entity.ApplyInfoAttach;

public interface ApplyInfoAttachMapper extends BaseMapper<ApplyInfoAttach> {

	List<ApplyInfoAttach> queryListByPage(Map<String, Object> entity);
	
	ApplyInfoAttach queryApplyInfoAttachByApplyId(String applyId);
}