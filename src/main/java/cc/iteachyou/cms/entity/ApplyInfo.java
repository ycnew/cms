package cc.iteachyou.cms.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;


@Data
@Table(name = "APPLY_INFO")
public class ApplyInfo implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "APPLY_ID")
    private String applyId;

	@Column(name = "APPLY_USER")
    private String applyUser;

	@Column(name = "APPLY_MOBILE")
    private String applyMobile;
	
	@Column(name = "APPLY_TITLE")
    private String applyTitle;
	
	@Column(name = "APPLY_DATE")
    private Date applyDate;
	
	@Column(name = "APPLY_STATE")
    private String applyState;
	
	@Column(name = "APPLY_REJECT_INFO")
    private String applyRejectInfo;
	
}