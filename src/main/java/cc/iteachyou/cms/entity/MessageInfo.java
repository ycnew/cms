package cc.iteachyou.cms.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/*
CREATE TABLE MESSAGE_INFO(
	MSG_ID VARCHAR(50) PRIMARY KEY,
	MSG_TITLE VARCHAR(50) NOT NULL,
	MSG_CONTENT VARCHAR(50) NOT NULL
) 
 */

@Data
@Table(name = "MESSAGE_INFO")
public class MessageInfo implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "MSG_ID")
    private String msgId;

	@Column(name = "MSG_TITLE")
    private String msgTitle;

	@Column(name = "MSG_CONTENT")
    private String msgContent;
	
}