package cc.iteachyou.cms.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;


@Data
@Table(name = "APPLY_INFO_ATTACH")
public class ApplyInfoAttach implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "ATTACH_ID")
    private String attachId;

	@Column(name = "ATTACH_FILE_NAME")
    private String attachFileName;

	@Column(name = "ATTACH_REL_PATH")
    private String attachRelPath;
	
	@Column(name = "APPLY_ID")
    private String applyId;
	
}