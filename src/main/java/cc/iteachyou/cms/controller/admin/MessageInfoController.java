package cc.iteachyou.cms.controller.admin;

import java.util.Date;
import java.util.List;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.github.pagehelper.PageInfo;

import cc.iteachyou.cms.annotation.Log;
import cc.iteachyou.cms.annotation.Log.OperatorType;
import cc.iteachyou.cms.common.ExceptionEnum;
import cc.iteachyou.cms.common.SearchEntity;
import cc.iteachyou.cms.entity.Field;
import cc.iteachyou.cms.entity.Form;
import cc.iteachyou.cms.entity.MessageInfo;
import cc.iteachyou.cms.exception.AdminGeneralException;
import cc.iteachyou.cms.exception.CmsException;
import cc.iteachyou.cms.exception.TransactionException;
import cc.iteachyou.cms.security.token.TokenManager;
import cc.iteachyou.cms.service.MessageInfoService;
import cc.iteachyou.cms.utils.UUIDUtils;

@Controller
@RequestMapping("admin/messageInfo")
public class MessageInfoController {
	
	@Autowired
	private MessageInfoService messageInfoService;
	
	@RequestMapping("/toAdd")
	@RequiresPermissions("w0kbe38p")
	public String toAdd() {
		return "admin/messageInfo/add";
	}
	
	@RequestMapping({"","/list"})
	@RequiresPermissions("7pswu738")
	public String toIndex(Model model ,SearchEntity params) {
		PageInfo<MessageInfo> messageInfos = messageInfoService.queryListByPage(params);
		model.addAttribute("page", messageInfos);
		return "admin/messageInfo/list";
	}
	
	@RequestMapping("/add")
	@RequiresPermissions("fn9o6433")
	public String add(Model model,MessageInfo messageInfo) {
		messageInfo.setMsgId(UUIDUtils.getPrimaryKey());
		messageInfoService.add(messageInfo);
		return "redirect:/admin/messageInfo/list";
	}
	
	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	@RequiresPermissions("3kc86164")
	public String delete(String msgId) throws CmsException {
		messageInfoService.delete(msgId);
		return "redirect:/admin/messageInfo/list";
	}
	
	@RequestMapping(value = "/toEdit", method = RequestMethod.GET)
	@RequiresPermissions("u51mogha")
	public String toEdit(Model model, String msgId) {
		MessageInfo messageInfo = messageInfoService.queryMessageInfoById(msgId);
		model.addAttribute("messageInfo", messageInfo);
		return "admin/messageInfo/edit";
	}
	
	@RequestMapping("/edit")
	@RequiresPermissions("19wh2wrf")
	public String edit(Model model,MessageInfo messageInfo) throws CmsException {
		messageInfoService.update(messageInfo);
		return "redirect:/admin/messageInfo/list";
	}
}
