package cc.iteachyou.cms.controller.admin;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.github.pagehelper.PageInfo;

import cc.iteachyou.cms.common.SearchEntity;
import cc.iteachyou.cms.entity.ApplyInfo;
import cc.iteachyou.cms.entity.ApplyInfoAttach;
import cc.iteachyou.cms.exception.CmsException;
import cc.iteachyou.cms.service.ApplyInfoAttachService;
import cc.iteachyou.cms.service.ApplyInfoService;
import cc.iteachyou.cms.utils.UUIDUtils;

@Controller
@RequestMapping("admin/applyInfo")
public class ApplyInfoController {
	
	@Value("${web.resource-path}")
	private String webResourcePath;
	
	@Autowired
	private ApplyInfoService applyInfoService;
	
	@Autowired
	private ApplyInfoAttachService applyInfoAttachService;
	
	@RequestMapping({"","/list"})
	@RequiresPermissions("7pswu738")
	public String list(Model model ,SearchEntity params) {
		PageInfo<ApplyInfo> applyInfos = applyInfoService.queryListByPage(params);
		model.addAttribute("page", applyInfos);
		return "admin/applyInfo/list";
	}
	
	@RequestMapping("/toAdd")
	@RequiresPermissions("w0kbe38p")
	public String toAdd() {
		return "admin/applyInfo/add";
	}
	
	@RequestMapping("/add")
	@RequiresPermissions("fn9o6433")
	public String add(Model model, ApplyInfo applyInfo, MultipartFile uploadFile) throws IllegalStateException, IOException {
		applyInfo.setApplyId(UUIDUtils.getPrimaryKey());
		applyInfo.setApplyDate(new Date());
		applyInfo.setApplyState("未审核");
		applyInfoService.add(applyInfo);
		
		ApplyInfoAttach applyInfoAttach = new ApplyInfoAttach();
		applyInfoAttach.setAttachId(UUIDUtils.getPrimaryKey());
		applyInfoAttach.setAttachFileName(uploadFile.getOriginalFilename());
		String fullPath = webResourcePath + "uploads/" + uploadFile.getOriginalFilename();
		applyInfoAttach.setAttachRelPath(fullPath);
		applyInfoAttach.setApplyId(applyInfo.getApplyId());
		applyInfoAttachService.add(applyInfoAttach);
		
		uploadFile.transferTo(new File(fullPath));
		
		model.addAttribute("applyId", applyInfo.getApplyId());
		
		return "admin/applyInfo/success";
	}

	@RequestMapping(value = "/searchById")
	@RequiresPermissions("3kc86164")
	public String searchById(Model model, String applyId) {
		ApplyInfo applyInfo = applyInfoService.queryApplyInfoById(applyId);
		model.addAttribute("applyInfo", applyInfo);
		return "admin/applyInfo/result";
	}
	
	
//	@RequestMapping(value = "/delete", method = RequestMethod.GET)
//	@RequiresPermissions("3kc86164")
//	public String delete(String msgId) throws CmsException {
//		messageInfoService.delete(msgId);
//		return "redirect:/admin/messageInfo/list";
//	}
	
	@RequestMapping(value = "/toEdit", method = RequestMethod.GET)
	@RequiresPermissions("u51mogha")
	public String toEdit(Model model, String applyId) {
		ApplyInfo applyInfo = applyInfoService.queryApplyInfoById(applyId);
		model.addAttribute("applyInfo", applyInfo);
		return "admin/applyInfo/edit";
	}
	
	@RequestMapping("/edit")
	@RequiresPermissions("19wh2wrf")
	public String edit(Model model,ApplyInfo applyInfo) throws CmsException {
		applyInfoService.update(applyInfo);
		return "redirect:/admin/applyInfo/list";
	}
	
	@RequestMapping(value = "/download", method = RequestMethod.GET)
	@RequiresPermissions("u51mogha")
	public void download(String applyId, HttpServletResponse response) throws Exception {
		ApplyInfoAttach applyInfoAttach = applyInfoAttachService.queryApplyInfoAttachByApplyId(applyId);

		response.reset();
		response.setContentType("application/octet-stream;charset=utf-8");
		response.setHeader("Content-disposition", "attachment; filename=" + applyInfoAttach.getAttachFileName());
		
		BufferedInputStream bis = new BufferedInputStream(new FileInputStream(applyInfoAttach.getAttachRelPath()));
		BufferedOutputStream bos = new BufferedOutputStream(response.getOutputStream());
		byte[] bs = new byte[1024];
		int len = 0;
		while( (len = bis.read(bs)) > 0) {
			bos.write(bs, 0, len);
		}
	}
}
