package cc.iteachyou.cms.controller.admin;

import cc.iteachyou.cms.annotation.Log;
import cc.iteachyou.cms.annotation.Log.OperatorType;
import cc.iteachyou.cms.common.BaseController;
import cc.iteachyou.cms.common.ExceptionEnum;
import cc.iteachyou.cms.entity.Attachment;
import cc.iteachyou.cms.entity.System;
import cc.iteachyou.cms.exception.AdminGeneralException;
import cc.iteachyou.cms.service.AttachmentService;
import cc.iteachyou.cms.service.SystemService;
import cc.iteachyou.cms.utils.FileConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.yaml.snakeyaml.util.UriEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;

/**
 * 附件管理
 * @author Wangjn
 * @version 2.0.0
 */
@Controller
@RequestMapping("/attachment/manage")
public class AttachmentDownloadController extends BaseController {
	
	@Autowired
	private FileConfiguration fileConfiguration;
	
	@Autowired
	private AttachmentService attachmentService;
	
	@Autowired
	private SystemService systemService;

	@Log(operType = OperatorType.OTHER, module = "附件管理", content = "下载附件")
	@RequestMapping("/download")
	public void download(String id,HttpServletRequest request,HttpServletResponse response) throws AdminGeneralException {
		try {
			System system = systemService.getSystem();
			Attachment attachment = attachmentService.queryAttachmentById(id);
		    //设置响应头和客户端保存文件名
		    response.setCharacterEncoding("utf-8");
		    response.setContentType("multipart/form-data");
		    response.setHeader("Content-Disposition", "attachment;fileName=" + URLEncoder.encode(attachment.getFilename(), "UTF-8"));
	        //打开本地文件流
		    String filePath = fileConfiguration.getResourceDir() + system.getUploaddir() + attachment.getFilepath();
	        InputStream inputStream = new FileInputStream(filePath);
	        //激活下载操作
	        OutputStream os = response.getOutputStream();
	        //循环写入输出流
	        byte[] b = new byte[1024];
	        int length;
	        while ((length = inputStream.read(b)) > 0) {
	            os.write(b, 0, length);
	        }
	        // 这里主要关闭。
	        os.close();
	        inputStream.close();
		}catch (Exception e) {
			throw new AdminGeneralException(
					ExceptionEnum.HTTP_INTERNAL_SERVER_ERROR.getCode(),
					ExceptionEnum.HTTP_INTERNAL_SERVER_ERROR.getMessage(),
					e.getMessage());
		}
	}
}
