package cc.iteachyou.cms.controller.msg;

import cc.iteachyou.cms.annotation.Log;
import cc.iteachyou.cms.annotation.Log.OperatorType;
import cc.iteachyou.cms.common.ExceptionEnum;
import cc.iteachyou.cms.common.SearchEntity;
import cc.iteachyou.cms.entity.Archives;
import cc.iteachyou.cms.entity.Category;
import cc.iteachyou.cms.entity.Field;
import cc.iteachyou.cms.entity.Form;
import cc.iteachyou.cms.entity.System;
import cc.iteachyou.cms.exception.AdminGeneralException;
import cc.iteachyou.cms.exception.CmsException;
import cc.iteachyou.cms.exception.TransactionException;
import cc.iteachyou.cms.security.token.TokenManager;
import cc.iteachyou.cms.service.ArchivesService;
import cc.iteachyou.cms.service.CategoryService;
import cc.iteachyou.cms.service.FieldService;
import cc.iteachyou.cms.service.FormService;
import cc.iteachyou.cms.service.LabelService;
import cc.iteachyou.cms.service.SystemService;
import cc.iteachyou.cms.utils.StringUtil;
import cc.iteachyou.cms.utils.UUIDUtils;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.checkerframework.checker.signature.qual.DotSeparatedIdentifiersOrPrimitiveType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 文章管理
 * @author Wangjn
 *
 */
@Controller
@RequestMapping("/cover-dzr40p3t")
public class MsgController {

	@Log(operType = OperatorType.PAGE, module = "留言填写", content = "列表")
	@RequestMapping("/tianxie")
	// mys9y5e4
	@RequiresPermissions("e6r77x94")
	public String toIndex(Model model, SearchEntity params) {
		return "portal/msg/index";
	}

	@ResponseBody
	@Log(operType = OperatorType.INSERT, module = "留言提交", content = "提交")
	@PostMapping("/addMsg")
	// mys9y5e4
	@RequiresPermissions("e6r77x94")
	public String addMsg(Model model,HttpServletRequest request, @RequestParam Map<String,String> entity) {
//		service.save();
		return "操作成功";
	}
}
