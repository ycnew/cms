package cc.iteachyou.cms.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cc.iteachyou.cms.common.SearchEntity;
import cc.iteachyou.cms.dao.ApplyInfoMapper;
import cc.iteachyou.cms.entity.ApplyInfo;
import cc.iteachyou.cms.service.ApplyInfoService;

@Service
@Transactional(rollbackFor = Exception.class) 
public class ApplyInfoServiceImpl implements ApplyInfoService {
	
	@Autowired
	private ApplyInfoMapper applyInfoMapper;
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED)
	@Override
	public PageInfo<ApplyInfo> queryListByPage(SearchEntity params) {
		if(params.getPageNum() == null || params.getPageNum() == 0) {
			params.setPageNum(1);
		}
		if(params.getPageSize() == null || params.getPageSize() == 0) {
			params.setPageSize(10);
		}
		PageHelper.startPage(params.getPageNum(), params.getPageSize());
		List<ApplyInfo> list = applyInfoMapper.queryListByPage(params.getEntity());
		PageInfo<ApplyInfo> page = new PageInfo<ApplyInfo>(list);
		return page;
	}

	@Override
	public int add(ApplyInfo applyInfo) {
		return applyInfoMapper.insert(applyInfo);
	}

//	@Override
//	public int delete(String msgId) {
//		return messageInfoMapper.deleteByPrimaryKey(msgId);
//	}

	@Override
	public ApplyInfo queryApplyInfoById(String applyId) {
		return applyInfoMapper.selectByPrimaryKey(applyId);
	}

	@Override
	public int update(ApplyInfo applyInfo) {
		return applyInfoMapper.updateByPrimaryKeySelective(applyInfo);
	}
}
