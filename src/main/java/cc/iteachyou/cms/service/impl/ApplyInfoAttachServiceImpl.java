package cc.iteachyou.cms.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cc.iteachyou.cms.dao.ApplyInfoAttachMapper;
import cc.iteachyou.cms.entity.ApplyInfoAttach;
import cc.iteachyou.cms.service.ApplyInfoAttachService;

@Service
@Transactional(rollbackFor = Exception.class) 
public class ApplyInfoAttachServiceImpl implements ApplyInfoAttachService {
	
	@Autowired
	private ApplyInfoAttachMapper applyInfoAttachMapper;	

	@Override
	public int add(ApplyInfoAttach applyInfoAttach) {
		return applyInfoAttachMapper.insert(applyInfoAttach);
	}

	@Override
	public ApplyInfoAttach queryApplyInfoAttachByApplyId(String applyId) {
		return applyInfoAttachMapper.queryApplyInfoAttachByApplyId(applyId);
	}
}
