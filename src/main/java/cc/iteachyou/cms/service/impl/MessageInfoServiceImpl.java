package cc.iteachyou.cms.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import cc.iteachyou.cms.common.SearchEntity;
import cc.iteachyou.cms.dao.MessageInfoMapper;
import cc.iteachyou.cms.entity.MessageInfo;
import cc.iteachyou.cms.service.MessageInfoService;

@Service
@Transactional(rollbackFor = Exception.class) 
public class MessageInfoServiceImpl implements MessageInfoService {
	
	@Autowired
	private MessageInfoMapper messageInfoMapper;
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED)
	@Override
	public PageInfo<MessageInfo> queryListByPage(SearchEntity params) {
		if(params.getPageNum() == null || params.getPageNum() == 0) {
			params.setPageNum(1);
		}
		if(params.getPageSize() == null || params.getPageSize() == 0) {
			params.setPageSize(10);
		}
		PageHelper.startPage(params.getPageNum(), params.getPageSize());
		List<MessageInfo> list = messageInfoMapper.queryListByPage(params.getEntity());
		PageInfo<MessageInfo> page = new PageInfo<MessageInfo>(list);
		return page;
	}

	@Override
	public int add(MessageInfo messageInfo) {
		return messageInfoMapper.insert(messageInfo);
	}

	@Override
	public int delete(String msgId) {
		return messageInfoMapper.deleteByPrimaryKey(msgId);
	}

	@Override
	public MessageInfo queryMessageInfoById(String msgId) {
		return messageInfoMapper.selectByPrimaryKey(msgId);
	}

	@Override
	public int update(MessageInfo messageInfo) {
		return messageInfoMapper.updateByPrimaryKey(messageInfo);
	}
}
