package cc.iteachyou.cms.service;

import cc.iteachyou.cms.entity.ApplyInfoAttach;

public interface ApplyInfoAttachService{

	int add(ApplyInfoAttach applyInfoAttach);
	
	ApplyInfoAttach queryApplyInfoAttachByApplyId(String applyId);

}
