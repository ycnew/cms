package cc.iteachyou.cms.service;

import com.github.pagehelper.PageInfo;

import cc.iteachyou.cms.common.SearchEntity;
import cc.iteachyou.cms.entity.ApplyInfo;

public interface ApplyInfoService{

	PageInfo<ApplyInfo> queryListByPage(SearchEntity params);

	int add(ApplyInfo applyInfo);
	
	
//	int delete(String msgId);
	
	ApplyInfo queryApplyInfoById(String applyId);
	
	int update(ApplyInfo applyInfo);
}
