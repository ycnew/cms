package cc.iteachyou.cms.service;

import com.github.pagehelper.PageInfo;

import cc.iteachyou.cms.common.SearchEntity;
import cc.iteachyou.cms.entity.MessageInfo;

public interface MessageInfoService{

	PageInfo<MessageInfo> queryListByPage(SearchEntity params);

	int add(MessageInfo messageInfo);
	
	int delete(String msgId);
	
	MessageInfo queryMessageInfoById(String msgId);
	
	int update(MessageInfo messageInfo);
}
