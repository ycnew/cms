# 数字政府门户

当前版本：1.0.0

1. 数字政府门户官网：http://124.71.57.48:8888
2. 数字政府门户管理后台：http://124.71.57.48:8888/admin/
- 管理员：admin
- 管理员密码：admin

数字政府门户-可以帮助政府灵活、准确、高效、智能地管理信息内容，实现信息的采集、加工、审核、发布、存储、检索、统计、分析、 反馈等整个信息生命周期的管理。采用时下最流行的Springboot+thymeleaf框架搭建，具有灵活小巧，配置简单，标签化模版，快速开发等特点。采用了解析式引擎与编译式引擎并存的模式，由于在解析模版时，解析式引擎拥有巨大的优势，但对于动态浏览的互动性质的页面，编译式引擎更实用高效，采用双引擎并存的模式，在保持标签风格一致性的同时，也保证将来开发更多互动模块时有更好的性能和更好的扩展

# 技术框架
* 核心框架：Spring Boot 2
* 安全框架：Apache Shiro 1.6
* 视图框架：Spring MVC 4
* 工具包：Hutool 5.3.7
* 持久层框架：MyBatis 3
* 日志管理：Logback
* 模版框架：Thymeleaf
* JS框架：jQuery，Bootstrap
* CSS框架：Bootstrap
* 富文本：Ueditor、editor.md
* Lombok


# 开发环境
* IDE：IntelliJ IDEA Community Edition
* DB：Mysql 5.7 +
* JDK：Jdk8
* Redis：3.2+
* LomBok 项目需要使用Lombok支持

# 工程开发步骤
1. 克隆项目到本地工作空间
2. 导入Eclipse或Sts等开发工具（推荐使用Spring Tools Suite 4），项目需要使用Lombok支持
3. 项目需要Redis，请自行修改application.yml中Redis配置
4. 修改项目资源目录，application.yml文件web.resource-path配置项（如D:/dreamer-cms/）
5. 导入数据库src/main/resources/db/db.sql，要求Mysql5.7版本，并修改application-(dev|prd).yml中数据配置
6. 将项目src/main/resources/db/cms.zip文件解压，保证解压后的目录路径的名称和资源目录一致
7. 运行项目MatchCMSApplicaton.java
8. 网站首页：https://localhost:8888 项目管理后台：https://localhost:8888/admin
9. 管理后台用户名：admin；密码：admin